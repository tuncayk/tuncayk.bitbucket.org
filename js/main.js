$(document).ready(function(){
        
    //Sosyal paylaşım butonları
    Socialite.load(); 

    //modal box'lar
    $('.open-popup-link').magnificPopup({
      type:'inline',
      closeMarkup: '<button onclick= "$.magnificPopup.close();" type="button" title="%title%" class="mfp-close"><i class="fa fa-times"></i></button>',
      midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    // slider
    $('.flexslider').flexslider({
      // animation: "slide"
      prevText: "",
      nextText: ""
    });

    //responsive menu
    var nav = responsiveNav(".nav-collapse", {
      // init: function(){}
    });

    //Tepe  yapışkan bar
    $("#top-bar-2").sticky({topSpacing:0});

  });
